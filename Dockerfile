FROM rust:1.70

WORKDIR /usr/src/app
COPY . .

RUN cargo install cobalt-bin
RUN ./git_log.sh
RUN cobalt build